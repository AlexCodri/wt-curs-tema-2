function addTokens(input, tokens){
    
    if(typeof input !=='string')
    throw new Error("Invalid input");
    
    if(input.length < 6)
    throw new Error("Input should have at least 6 characters");
    
    tokens.forEach(tokens => {
        if(typeof tokens.tokenName !=='string')
        throw new Error("Invalid array format");
    })
    
   if(input.indexOf("...")===-1)
   return input;
   

  tokens.forEach(tokens => {
        var replaceString = "${"+ tokens.tokenName +"}";
        input = input.replace('...', replaceString);
    });
    return input;
    
}

const app = {
    addTokens: addTokens
}

module.exports = app;